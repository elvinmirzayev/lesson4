package az.ingrees.demo;

public class Student {


    private static Student instance;

    private Student() {

    }

    public static Student getInstance() {
        if (instance==null) {
            synchronized (Student.class) {
                if (instance == null) {
                    instance = new Student();
                }
            }
        }
        return instance;
    }
}
