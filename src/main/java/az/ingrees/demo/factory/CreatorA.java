package az.ingrees.demo.factory;

public class CreatorA extends Creator{

@Override
    public Product createProduct(){
        return new ProductA();
    }
}
