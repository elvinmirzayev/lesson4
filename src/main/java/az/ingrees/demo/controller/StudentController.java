package az.ingrees.demo.controller;


import az.ingrees.demo.service.StudentService;
import az.ingrees.demo.service.StudentServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/student")

public class StudentController {
    @Autowired
    private StudentService studentService;
    @GetMapping("/{id}")
    public Student get(@PathVariable Long id){
        return studentService.get(id);
    }
    @PostMapping
    public Student create(@RequestBody Student student){
        return studentService.create(student);
    }

    @PutMapping
    public Student update(@RequestBody Student student){
        return studentService.update(student);
    }
    @DeleteMapping("/student{id}delete")
    public void delete(@PathVariable Long id){
        studentService.delete(id);
    }
}


